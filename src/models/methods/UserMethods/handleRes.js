
// secure my responseby by creating new obj from user in db with deleted stuff
module.exports = function (UserSchema) { 
    UserSchema.methods.toJSON = function () {
        const user = this
        const userObject = user.toObject()
    
        delete userObject.password
        delete userObject.tokens
        delete userObject.refreshTokens
        delete userObject.__v
    
        return userObject
}    }