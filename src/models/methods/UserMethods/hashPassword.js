//hash the plain text password befor saving or if the password modified 
const bcrypt = require('bcryptjs')
module.exports = function (UserSchema) {
    
    UserSchema.pre('save', async function (next) {
    const user = this
       if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 10)
       }
    next()
})
}