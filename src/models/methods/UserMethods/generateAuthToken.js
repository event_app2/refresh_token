// generate refresh_token and Id_token  using jwt token and then conact it to database in user.tokens and user.refreshtokens
const jwt = require('jsonwebtoken')
module.exports = function (UserSchema) {
    UserSchema.methods.generateAuthToken = async function () {
            const user = this
            const token =  jwt.sign({ _id: user._id.toString(), Name: user.Name, Email: user.email, ProfilePic: user.profilePic }, process.env.JWT_SECRET, { expiresIn: process.env.TokenTime })
        
            const refreshToken =  jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET, { expiresIn: process.env.TokenTimeRe })
            user.tokens = user.tokens.concat({ token , refreshToken })
            await user.save()
            
            return  { token , refreshToken}
    };
}

