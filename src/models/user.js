const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const coustmError = require('../utils/customErrors')

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        trim: true,
        max:32,
        required: true 

    },
    lastName: {
        type: String,
        trim: true,
        max: 32,
        required: true     
    },

    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
    },

    password: {
        type: String,
        required: true,
        maxlength: 64,
        minlength:8
    },

    birthday: {
        type: String,
        trim: true,
        required: true

    },   
    profilePic: {
        type: String,
        trim: true,
        default: "photo URL"
    },
    tokens: [{
        token: {
            type: String,
            required: true,
        },
        refreshToken: {
            type: String,
            required: true,
    }
    }],
}, {
    timestamps: true,
})


require('./methods/UserMethods/handleRes')(UserSchema)
require('./methods/UserMethods/generateAuthToken')(UserSchema)
require('./methods/UserMethods/hashPassword')(UserSchema)

UserSchema.statics.findByCredentials = async (email, password) => {
 
        const user = await User.findOne({ email })
        if (!user) {       
            throw new coustmError('Unable to log in',400)
        }
        const isMatch = await bcrypt.compare(password, user.password)
    
        if (!isMatch) {
            throw new coustmError('Unable to log in',400)
        }
        return user
}
const User = mongoose.model('Users', UserSchema)
module.exports = User
