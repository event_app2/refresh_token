const User = require('../models/user')
const E = require('../utils/StaticErrorMessage')
const customError = require('../utils/customErrors')
const asyncErrorWrapper = require('../utils/asyncErrorWrapper')

const UserSignUp = async (req, res, next) => {
        
        const user = new User({ ...req.body })
        const { token, refreshToken } = await user.generateAuthToken()
        await user.save()
        res.status(201).send({token, refreshToken })
}

const UserLogIn =  async (req, res,next) => {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const { token , refreshToken} = await user.generateAuthToken()
        res.status(200).send({user, token ,refreshToken})

}
const UserlogOut =  async (req, res,next) => {
        req.user.tokens = req.user.tokens.filter((tokens) => {
            return tokens.token !== req.token
        })
        await req.user.save()
        res.send()

}
const UserLogOutAll = async (req, res,next) => {
        req.user.tokens = []
        await req.user.save()
        res.send({ logoutALL: 'Logged out from all devices' })
}

module.exports = {
    UserSignUp,
    UserLogIn,
    UserlogOut,
    UserLogOutAll
}