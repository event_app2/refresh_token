const Refresh  = async(req, res) => {
    try {
        const { token, refreshToken} = await req.user.generateAuthToken()
        req.user.tokens = req.user.tokens.filter((tokens) => {
            return tokens.refreshToken !== req.refreshToken
        })
        await req.user.save()
       res.status(201).send({ token, refreshToken})
   } catch (error) {
    next(error)
   }
}

module.exports = {
    Refresh
}