const express = require('express')
const auth = require('../middleware/auth')
const router = new express.Router()
const ErrorWrapper = require("../utils/asyncErrorWrapper")
const UserController = require ('../controller/UserController')

router.post('/users/signup',ErrorWrapper(UserController.UserSignUp))

router.post('/users/login',ErrorWrapper( UserController.UserLogIn))

router.post('/users/logout', auth,ErrorWrapper( UserController.UserlogOut))

router.post('/users/logoutAll', auth,ErrorWrapper ( UserController.UserLogOutAll))

router.post('/user/events',auth, async (req, res,next) => {
    res.send("hello bro")
})

module.exports = router