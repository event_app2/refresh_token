const refreshTokenAuth = require ('../middleware/refreshTokenAuth')
const express = require('express')
const RefreshController = require('../controller/RefreshToken')
const ErrorWrapper = require ('../utils/asyncErrorWrapper') 
const router = new express.Router()

router.post('/refreshToken', refreshTokenAuth,ErrorWrapper( RefreshController.Refresh))

router.get('/hi', async (req, res) => {
    res.send('hello happy mushroom')
    
})

module.exports = router