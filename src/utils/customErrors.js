class customError extends Error {
    constructor(  message, statusCode,customErrorCode = 0 ) {
        super(message);
        this.customErrorCode = customErrorCode
        this.statusCode = statusCode 
        this.status = statusCode >= 400 && statusCode < 500 ? 'Fail' : 'Something went wrong in the server please try again later'
        this.isOperational = true 
        Error.captureStackTrace(this,this.constructor)
    }   
}

module.exports = customError