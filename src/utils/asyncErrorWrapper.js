
const E = require("./StaticErrorMessage")
const customError = require("./customErrors")


const asyncErrorHandler = (func) => {

    return (req, res, next) => {        
        func(req, res, next).catch(err => {
            console.log(err.message)
            switch (true) {
                case (err.code === 11000):
                    const DuplicatedValue = new customError(E.Already, 400)
                    return next(DuplicatedValue)
                case (err.name === 'ValidationError'):
                    const ValidationError = new customError(err.message, 400);
                    return next(ValidationError)
                case(err.name === "TokenExpiredError") :
                    let TokenExpiredError = new customError(E.AccessTokenexpired, 401, 10)
                    return next(TokenExpiredError)
                case (err.message === "invalid signature"):
                    const TokenInvalidSig = new customError(E.TokenInvalidSig, 401)
                    return    next (TokenInvalidSig)
                default:
                    return next(err)
            }
        })
    }
}

module.exports = asyncErrorHandler