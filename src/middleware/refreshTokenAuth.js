const User = require("../models/user");
const jwt = require("jsonwebtoken");
const customError = require("../utils/customErrors");
const E = require("../utils/StaticErrorMessage");
const refreshTokenAuth = async (req, res, next) => {
  try {
    const refreshToken = req.body.refreshToken;
    const decoded = jwt.verify(refreshToken, process.env.JWT_SECRET);
    const user = await User.findOne({
      _id: decoded._id,
      "tokens.refreshToken": refreshToken,
    });
    if (!user) {
      throw new customError(E.RfreshTokNotValid, 400);
    }
    req.refreshToken = refreshToken;
    req.user = user;
    next();
  } catch (Error) {
    console.log(Error);
    if (Error.name === "TokenExpiredError") {
      const err = new customError(E.RfreshTokexpir, 401, 11);
      next(err);
    } else if (Error.message === "invalid signature") {
        const err = new customError(E.TokenInvalidSig, 401)
        next (err)
    } else {
      next(Error);
    }
  }
};
module.exports = refreshTokenAuth;
