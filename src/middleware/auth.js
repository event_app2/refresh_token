const jwt = require('jsonwebtoken');
const User = require('../models/user');
const customError = require('../utils/customErrors');
const E = require('../utils/StaticErrorMessage');
const asyncErrorHandler = require('../utils/asyncErrorWrapper');

const auth = asyncErrorHandler( async (req,res,next) =>{
    const token = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(token, process.env.JWT_SECRET)
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })
        console.log(user)
        if(!user){
            const err = new customError(E.AccessTokenNotValid, 400)
            next(err)
        }
        req.token = token
        req.user=user
        next()   
})

module.exports = auth 