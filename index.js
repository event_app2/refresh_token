const express = require('express')
require('./src/db/mongoose')
const tokenRouter = require('./src/routers/token')
const userRouter = require('./src/routers/user')
const errorHandler = require('./src/controller/errors/error_handler')

const app = express()
const port = process.env.PORT

app.use(express.json())
app.use(tokenRouter)
app.use(userRouter)
app.use(errorHandler)
app.listen(port, () => {
    console.log('Server is up on port ' + port)
})


